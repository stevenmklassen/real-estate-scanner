import cfscrape  # See docs: https://pypi.org/project/cfscrape/  OR  https://github.com/Anorov/cloudflare-scrape
from requests_html import HTMLSession, HTMLResponse
import time
import json
from datetime import datetime
import re

# TODO: Categorize different listing types and what data they have
# TODO: Check a variety of listings to see where the find-logic breaks
#   - Some Chilliwack listings don't include neighbourhoods
#   - Exclude listings that don't include bed/bath (Type: Vacant Land, which means RV)

debug_print = False
max_listings_per_city = 0

base_url = 'https://www.zolo.ca/'
end_url = '-real-estate/'

# Cities generally organized from East-to-West
cities = [
    'hope',
    'harrison-hot-springs',
    'chilliwack',
    'abbotsford',
    'mission',
    'langley',
    'maple-ridge',
    'pitt-meadows',
    'surrey',
    'coquitlam',
    'port-coquitlam',
    'white-rock',
    'port-moody',
    'new-westminster',
    'delta',
    'richmond',
    'burnaby',
    'vancouver',
    'north-vancouver',
]

session = HTMLSession()
scraper = cfscrape.create_scraper(session)    # returns a CloudflareScraper instance, wrapping an HTMLSession
total_listings_count = 0
city_listings_count = 0
page_count = 0
city_count_maxed = False

def scrape_listings(page_url, city_name, property_list=[]):
    
    print(f'SCRAPING!  URL: {page_url}')

    global city_count_maxed

    global page_count
    page_count = page_count + 1
    print("Scanning page #" + str(page_count) + "...")


    # Get listing URLs
    main_resp = scraper.get(page_url)
    b_content = main_resp.content
    content = str(b_content)
    link_tags = main_resp.html.find('a.card-listing--image-link')

    # Iterate using other function
    listing_resp = ''
    global total_listings_count
    global city_listings_count
    for tag in link_tags:

        link = tag.attrs['href']

        # Navigate to page and kick off data scrape
        total_listings_count = total_listings_count + 1
        city_listings_count = city_listings_count + 1

        print("Navigating to listing #" + str(total_listings_count) + ":", link)
        listing_resp = scraper.get(link)

        listing = get_property_data_from_response2(listing_resp)

        # Handle bad result
        if 'error' in listing.keys():
            print(" --> Skipped listing #" + str(total_listings_count))
            print("     ERROR:", listing['error'])
            continue

        listing['url'] = link
        property_list.append(listing)

        # If maxed on listings count, break loop
        global max_listings_per_city
        if max_listings_per_city != 0 and city_listings_count >= max_listings_per_city:
            city_count_maxed = True
            break
        # time.sleep(1)

    # Check if loop ended because of maximum allowed properties per city
    if city_count_maxed:
        print(f'Ending property scan for city "{city_name}" due to maximum count.')
        return property_list

    try:
        next_page_button = main_resp.html.find('section.supplementary-nav a.button[aria-label="next page of results"]', first=True)
        if next_page_button:
            next_page_url = next_page_button.attrs['href']
            print(f'Navigating to next page... ({next_page_url})')
            scrape_listings(next_page_url, city_name, property_list)
    except:
        if debug_print:
            print('(No next-page-button. Returning results...)')

    return property_list



def get_property_data_from_response(resp):

    # Get listing data into vars
    # TODO: Split the below functions into 3 groups (above buttons, 1st batch, 2nd batch)
    address = resp.html.find('h1.address', first=True).text
    price = resp.html.find('section.listing-price > div.heavy', first=True).text
    city_listed = resp.html.find('section.listing-location > div > a:nth-child(1)', first=True).text
    neighbourhood = resp.html.find('section.listing-location > div > a:nth-child(2)', first=True).text
    bed_bath_sqft = resp.html.find('section.listing-location > ul', first=True)
    beds = bed_bath_sqft.find('ul > li:nth-child(1)', first=True).text
    baths = bed_bath_sqft.find('ul > li:nth-child(2)', first=True).text
    sqft = bed_bath_sqft.find('ul > li:nth-child(3)', first=True).text
    desc = resp.html.find('div.section-listing-content-pad > span:nth-child(1).priv', first=True).text.split('\n')[0]
    building_type = resp.html.find('section.section-listing-content > div:nth-child(1) > dl:nth-child(1) > dd', first=True).text
    built_year = resp.html.find('section.section-listing-content > div:nth-child(1) > dl:nth-child(4) > dd', first=True).text
    listed = resp.html.find('section.section-listing-content > div:nth-child(2) > dl:nth-child(1) > dd', first=True).text
    mls_id = resp.html.find('section.section-listing-content > div:nth-child(2) > dl:nth-child(4) > dd', first=True).text
    
    # Date handling
    date_format = '%Y-%m-%d'
    
    listed_date_str = listed.split(' (')[0]
    listed_date = datetime.strptime(listed_date_str, '%b %d, %Y').strftime(date_format)

    accessed_date = datetime.strftime(datetime.now(), date_format)

    # Conditional properties for apartments, townhomes
    strata_fees = None
    if building_type == 'Apartment/Condo':
        strata_fees = resp.html.find('section.section-listing-content > div:nth-child(1) > dl:nth-child(7) > dd', first=True).text

    # Compile master dict to return
    listing_details = {
        'city': city_listed,
        'address': address,
        'price': price,
        'neighbourhood': neighbourhood,
        'beds': beds,
        'baths': baths,
        'sqft': sqft,
        'desc': desc,
        'building_type': building_type,
        'strata_fees': strata_fees,
        'built_year': built_year,
        'listed_date': listed_date,
        'accessed_date': accessed_date,
        'mls_id': mls_id
    }
    
    if debug_print:
        print("Listing details:", json.dumps(listing_details, sort_keys=False, indent=4))

    return listing_details

def get_property_data_from_response2(resp):

    # Check for Inactive property, return error
    price_or_status = resp.html.find('section.listing-price', first=True).text
    status_check = re.search("^(\w+)", price_or_status)
    # print("status_check:", status_check)
    if status_check and str(status_check[0]) in ["Removed", "Inactive"]:
        
        return {'error': f'Listing no longer active ("{price_or_status}")'}

    # Get data from summary (under images, above first buttons)
    address = resp.html.find('h1.address', first=True).text
    price = resp.html.find('section.listing-price > div.heavy', first=True).text
    city_listed = resp.html.find('section.listing-location > div > a:nth-child(1)', first=True).text
    baths = resp.html.find('section.listing-location > ul > li:nth-child(2)', first=True).text
    desc = resp.html.find('div.section-listing-content-pad > span:nth-child(1).priv', first=True).text.split('\n')[0]

    # Define master dictionary
    property_details = {
        'Address': address,
        'Price': price,
        'City_listed': city_listed,
        'Baths': baths,
        'Description': desc
    }

    # Get data from 1st batch (Summary)
    listing_data_rows1 = resp.html.find('section.section-listing-content dl.column')
    listing_data1 = {}

    for row in listing_data_rows1:
        key, val = row.text.split('\n')  # Gets key/value pair from label
        listing_data1[key] = val


    # Get data from 2nd batch (More facts and features)
    listing_data_rows2 = resp.html.find('div.section-listing-content-pad section.column-gap div.column')
    listing_data2 = {}

    for row in listing_data_rows2:
        if row.text.find('\n') > 0:
            key, val = row.text.split('\n')  # Gets key/value pair from label
            listing_data2[key] = val
        else:
            listing_data2[row.text] = ''

    

    # Check property type to see if we should skip
    blocked_types = [
        'Recreational',
        'Vacant Land'
    ]
    if listing_data1['Type'] in blocked_types:
        return {'error': 'Listing is blocked type "' + listing_data1['Type'] + '"'}


    # Parse values to better identify them
    # -- Date handling
    date_format = '%Y-%m-%d'
    
    added_date_str = listing_data1['Added'].split(' (')[0]
    added_date = datetime.strptime(added_date_str, '%b %d, %Y').strftime(date_format)
    listing_data1['Added'] = added_date
    
    updated_date_str = listing_data1['Updated'].split(' (')[0]
    updated_date = datetime.strptime(updated_date_str, '%b %d, %Y').strftime(date_format)
    listing_data1['Updated'] = updated_date

    accessed_date = datetime.strftime(datetime.now(), date_format)
    
    # Trim duplicate / unused properties
    listing_data1.pop('Type', None)  # Defined in list 2
    listing_data1.pop('Style', None)  # Defined in list 2
    listing_data1.pop('Last Checked', None)
    listing_data2.pop('Size (sq ft)', None)  # Defined in list 1

    # -- Renaming keys
    listing_data1['MLS ID'] = listing_data1.pop('MLS\u00ae#', None)
    listing_data2['Property Type'] = listing_data2.pop('Type', None)

    # Combine into master dictionary

    property_details.update(listing_data1)
    property_details.update(listing_data2)
    property_details['Accessed'] = accessed_date
    
    if debug_print:
        print("Listing details:", json.dumps(property_details, sort_keys=False, indent=4))

    return property_details


def write_output(content, filename):
    pretty_content = json.dumps(content, sort_keys=False, indent=4)

    # filename = 'files\\data\\listings_' + now + '.json'
    if debug_print:
        print(f'\nWriting contents to "{filename}"\n')

    with open(filename, "w") as f:
        f.write(pretty_content)


def full_run():

    properties_by_city = {}
    dt_now = datetime.now()
    now = datetime.strftime(dt_now, '%Y%m%d_%H%M%S')
    filename = 'files\\data\\listings_' + now + '.json'

    print(f'Starting scan at {dt_now}...')

    global city_listings_count
    global page_count
    global city_count_maxed

    for city_name in cities:

        city_url = base_url + city_name + end_url

        properties_by_city[city_name] = {
            'city_name': city_name,
            'city_url': city_url,
            'listings': []
        }
        current_listings = properties_by_city[city_name]['listings']

        scrape_listings(city_url, city_name, current_listings)
        
        print(f'Completed scan of "{city_name}" with {city_listings_count} ({len(current_listings)}) listings over {page_count} pages.')
        write_output(properties_by_city, filename)
        
        # Reset variables for next iteration
        city_listings_count = 0  # Reset city listing counter
        page_count = 0  # Reset page counter
        city_count_maxed = False


    print(f'All {len(cities)} cities scanned!\n')

    pretty_result = json.dumps(properties_by_city, sort_keys=False, indent=4)

    # print("Final result:", pretty_result)
    print(f'\n\nSee final output in {filename}')


def test_run(url):
    
    listing_resp = scraper.get(url)

    listing = get_property_data_from_response2(listing_resp)
    print('Test result:', listing)

full_run()
# test_run('https://www.zolo.ca/maple-ridge-real-estate/22291-116-avenue/22289-')
