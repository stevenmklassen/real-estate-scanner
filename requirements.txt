Scrapy>=1.7.3
selenium>=3.141.0
cfscrape>=2.1.1
seaborn>=0.12.1
matplotlib>=3.6.0