---------------------------------

TO RUN SCRAPING SCRIPTS (PYTHON)

---------------------------------

0.  Download appropriate chromedriver.exe matching your version of Chrome here:
    https://chromedriver.chromium.org/downloads

1.  Open an administrative command prompt into the local project folder:
	- Win + R
	- "cmd"
	- Ctrl + Shift + Enter
	- cd <dir>

2.  Set up Python virtual environment
	- python -m venv venv

3.  Activate virtual environment
	- env\Scripts\activate.bat

    Prompt should now have prefix (env)

4.  Install required libraries
	- pip install -r requirements.txt


Scrapy:

5a. To start a spider run `scrapy crawl <spidername>`
5b. To start the shell for testing commands, run `scrapy shell <url>`