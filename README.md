# Real Estate Scanner

## Preview (TODO)
![Scatter plot: listing price by size (sq ft)](/files/graph_scatter_price_by_size.png)

## Description
The goal of this project is to interpret real estate market data (active or historical) in British Columbia's Metro Vancouver and Fraser Valley regions. This will be accomplished through 3 steps:
1. Collect and parse data from real estate websites (selected zolo.ca)
2. Clean and combine the data into a single useful format (currently JSON/Pandas; SQL in the future)
3. Present the data in an easy-to-consume dashboard (currently Jupyter notebook)

Right now this project is a placeholder for these ideas. The README should be updated as changes are made.

## Installation
(For Windows, Python)
1. [Download appropriate chromedriver.exe](https://chromedriver.chromium.org/downloads) matching your version of Chrome
2. python -m venv venv
3. venv\Scripts\activate.bat
4. pip install -r requirements.txt

## Usage (TODO)
1. Run the website scraper by calling `python zolo_scraper.py` (NOTE: This can take up to 2 hours with full city listings)
2. Rename the final output file to `property_listings_master.json`
3. Run the Python notebook `REscan_analysis.ipynb`

## Roadmap
- Convert JSON format into multi-table SQL database
- Add models based on available data
- Add and compare data from real estate boards (CREA, FVREB, etc.)

## License
This project is licensed under the terms of the MIT license.
