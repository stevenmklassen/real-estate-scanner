import os, json, re

file_path = './files/data/'

def main():

    # 1. Read list of files
    #    Get newest-to-oldest list of JSON files in file_path
    json_files = [json_file for json_file in os.listdir(file_path) if json_file.endswith('.json')]
    json_files.reverse() # Sort newest-to-oldest
    print(str(len(json_files)) + " files found:", json.dumps(json_files, sort_keys=False, indent=4))
    print()

    # 2. Select files by same date (not time)

    # First ask to combine files with most recent date
        # Get date of first (newest) file
    date_str = ""
    date_search = re.search("^listings_(\d+)_.+\.json$", json_files[0])
    if date_search and date_search[1]:
        date_str = ymd_str_to_date(date_search[1])
        print("Most recent date:", date_str)
        print()
    else:
        print("No date found, ending script...")
        print()
        quit()

    date_resp = ""
    while (date_resp not in ['y', 'n']):
        date_resp = input(f"Would you like to combine all listings scraped on {date_str}? (Y/N): ").lower()
        print()

    matching_files = []
    
    if date_resp == 'y':
        # Get files matching date_search[1]
        
        for f in json_files:
            if f.startswith('listings_' + date_search[1]):
                matching_files.append(f)
        
    elif date_resp == 'n':
        # Display files with index numbers (last-to-first)
        print("ERROR: Custom matching functionality not implemented.\nPlease contact the project owner or remove newer files.")
        quit()

    print("Combining " + str(len(matching_files)) + " files found:", json.dumps(matching_files, sort_keys=False, indent=4))
    
    # 3. Read & combine files (based on LAST date of file)
    combine_listings(matching_files)


## FUNCTIONS

# Takes an 8-digit YYYYMMDD string and converts to YYYY-MM-DD
def ymd_str_to_date(ymd_str):
    # print("Combining number:", ymd_str)
    result = ymd_str[0:4] + "-" + ymd_str[4:6] + "-" + ymd_str[6:8]
    return result


# Combines JSON files listed in filenames by unique city contents.
# TODO: Add combination logic for matching cities, selecting newest based on Updated date.
def combine_listings(filenames):
    num_files = len(filenames)
    if num_files == 1:
        print(f"Found {num_files} file, no combination performed.\nReturning file.")
        return filenames[0]

    elif num_files == 0:
        print("ERROR! Blank list passed to combine_listings(), ending program...")
        quit()

    # json_file = open(filename)
    # json_text = json.load(json_file)
    # print(json.dumps(json_text, indent=4)[:200])

    master_json = json.load(open(file_path + filenames[0]))
    print("Original file cities:", master_json.keys())

    for fn in filenames[1:]:

        f = json.load(open(file_path + fn))

        ## First, check by city (root string property). If unique, then combine them
        for city in f.keys():

            if city not in master_json.keys():
                # Unique, just add
                master_json[city] = f[city]
            else:
                print('TODO - Add logic for combining matched cities based on updated date. Ending program...')
                quit()


            ## If not a unique city, prompt about combining based on "listing[#].Updated"
    
    print("Final file cities:", master_json.keys())

    path_match = re.search("^(listings_\d+_)", filenames[0])
    final_path = ""

    if path_match and path_match[1]:
        final_path = file_path + path_match[1] + "combined.json"
        print("Saving as '" + final_path + "'")
    else:
        final_path = file_path + "listings_combined.json"
        print("ERROR! Couldn't adopt most recent filename.\nSaving as '" + final_path + "'")

    pretty_content = json.dumps(master_json, sort_keys=False, indent=4)
    with open(final_path, "w") as final_json:
        final_json.write(pretty_content)


## RUNTIME

main()